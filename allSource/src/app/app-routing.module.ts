import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AnalysisPageComponent } from './components/analysis-page/analysis-page.component';
import { DonatorsPageComponent } from './components/donators-page/donators-page.component';
import { DonorDetailsComponent } from './components/matching-page/donor-row/donor-details/donor-details.component';
import { MatchingPageComponent } from './components/matching-page/matching-page.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { StartPageComponent } from './components/start-page/start-page.component';

const routes: Routes = [
    {path: '', redirectTo: '/start-page', pathMatch: 'full'},
    {path: 'start-page', component: StartPageComponent},
    {path: 'analysis-page', component: AnalysisPageComponent},
    {path: 'donator/:id', component: DonorDetailsComponent},
    {path: 'matching-page', component: MatchingPageComponent},
    {path: 'donators-page', component: DonatorsPageComponent},
    { path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }