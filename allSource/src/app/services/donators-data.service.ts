import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DonatorStructure } from '../models/donators-structure';
import { tap } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DonatorsDataService {
  [x: string]: any;

  private url = 'http://localhost:3000';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      Authorization: 'my-auth-token'
    })
  };

  constructor(private httpClient: HttpClient) {}

  getDonators(): Observable<DonatorStructure[]> {
    return this.httpClient.get<DonatorStructure[]>(this.url + '/donatorsList');
  }

  getDonator(id: string): Observable<DonatorStructure> {
    return this.httpClient.get<DonatorStructure>(this.url + '/donatorsList/' + id);
  }

  deleteDonator(id: string): Observable<{}>{
    return this.httpClient.delete<{}>(this.url + '/donatorsList/' + id)
      .pipe(tap(console.log));
  }

  patchDonator(donator: Partial<DonatorStructure>): Observable<DonatorStructure>{
    return this.httpClient.patch<DonatorStructure>(this.url + '/donatorsList/' + donator.id, donator)
      .pipe(tap(console.log));
  }

  getAntigen(): Observable<string[]> {
    return this.httpClient.get<string[]>(this.url + '/antigenCategories');
  }

  getBlood(): Observable<string[]> {
    return this.httpClient.get<string[]>(this.url + '/bloodCategories');
  }

  getGender(): Observable<string[]> {
    return this.httpClient.get<string[]>(this.url + '/genderCategories');
  }

  postDonator(donator: DonatorStructure): Observable<DonatorStructure> {
    return this.httpClient.post<DonatorStructure>(this.url+'/donatorsList', donator)
  }
  
  getDonorsFromCategory(antigenCategories: string): Observable<DonatorStructure[]> {
    return this.getDonators().pipe(
      map(donors => donors.filter(donors => donors.antigen === antigenCategories))
    );
  }
}