import { ThisReceiver } from '@angular/compiler';
import { Component, Input, OnInit } from '@angular/core';
import { values } from 'lodash';
import { DonatorStructure } from 'src/app/models/donators-structure';
import { DonatorsDataService } from 'src/app/services/donators-data.service';

@Component({
  selector: 'app-start-page',
  templateUrl: './start-page.component.html',
  styleUrls: ['./start-page.component.css']
})
export class StartPageComponent{

  countDonors: number=0;
  listGender : (number|undefined)[] = [];
  listGroup : (number|undefined)[] = [];


  constructor(private donatorsDataService: DonatorsDataService) {
    donatorsDataService.getDonators().subscribe(
      donatorStreamData => {
        this.countDonors = donatorStreamData.length;
        this.listGender = this.countGender(donatorStreamData);
        this.listGroup = this.countGroup(donatorStreamData);
      }
    );
  }  

  countGender(donatorList: DonatorStructure[]){
    let countFemale:number = 0;
    let countMale:number = 0;
    for (let donator of donatorList){
      if (donator.gender === "kobieta"){
        countFemale++
      }
      else {
        countMale++
      }
    }
    let listGender = [countFemale, countMale];
    return listGender;
  }
  

  countGroup(donatorList: DonatorStructure[]){
    let countOm:number = 0;
    let countOp:number = 0;
    let countAm:number = 0;
    let countAp:number = 0;
    let countBm:number = 0;
    let countBp:number = 0;
    let countABm:number = 0;
    let countABp:number = 0;
    for (let donator of donatorList){
      if(donator.blood==="0"){
        if(donator.antigen==="-"){
          countOm++
        }
        else countOp++
      }
      else if(donator.blood==="A"){
        if(donator.antigen==="-"){
          countAm++
        }
        else countAp++
      }
      else if(donator.blood==="B"){
        if(donator.antigen==="-"){
          countBm++
        }
        else countBp++
      }
      else if(donator.blood==="AB"){
        if(donator.antigen==="-"){
          countABm++
        }
        else countABp++
      }
    }
    let listGroup=[countOm, countOp, countAm, countAp, countBm, countBp, countABm, countABp];
    return listGroup;
 }
      

}