import { Component, OnInit } from '@angular/core';
import { DonatorStructure } from 'src/app/models/donators-structure';
import { DonatorsDataService } from 'src/app/services/donators-data.service';

@Component({
  selector: 'app-gender-pie',
  templateUrl: './gender-pie.component.html',
  styleUrls: ['./gender-pie.component.css']
})
export class GenderPieComponent{
  public chartType: string = 'pie';
  donatorCount:number = 0;
  full: number = 0;
  listGender: (number|undefined)[] = [];
  chartDatasets: Array<any> = [
    { data: [], label: 'Grupy krwi' }
  ];
  

  constructor(private donatorsDataService: DonatorsDataService) {
    donatorsDataService.getDonators().subscribe(
      donatorStreamData => {
        this.donatorCount = donatorStreamData.length;
        this.listGender = this.countGender(donatorStreamData);
        this.chartDatasets = [
          { data: this.listGender, label: 'Płeć' }
        ];
      }
    );
  }  

  countGender (donatorList: DonatorStructure[]){
    let maleCount: number = 0;
    let femaleCount: number= 0;
    for(let donator of donatorList){
      if (donator.gender === "mężczyzna"){
          maleCount++
        }
        else {
          femaleCount++
        }
      }
    let listGender:Array<number> = [femaleCount, maleCount];
    return listGender;
  }


  public chartLabels: Array<any> = ['kobiety', 'mężczyźni'];

  public chartColors: Array<any> = [
    {
      backgroundColor: ['#F7464A', '#46BFBD'],
      hoverBackgroundColor: ['#FF5A5E', '#5AD3D1'],
      borderWidth: 2,
    }
  ];

  public chartOptions: any = {
    responsive: true
  };
  public chartClicked(e: any): void { }
  public chartHovered(e: any): void { }

}
