import { Component, OnInit } from '@angular/core';
import { DonatorStructure } from 'src/app/models/donators-structure';
import { DonatorsDataService } from 'src/app/services/donators-data.service';

@Component({
  selector: 'app-donors-blood-pie',
  templateUrl: './donors-blood-pie.component.html',
  styleUrls: ['./donors-blood-pie.component.css']
})
export class DonorsBloodPieComponent{
  public chartType: string = 'pie';
  donatorCount:number = 0;
  full: number = 0;
  listGroup: (number|undefined)[] = [];
  chartDatasets: Array<any> = [
    { data: [], label: 'Grupy krwi' }
  ];
  

  constructor(private donatorsDataService: DonatorsDataService) {
    donatorsDataService.getDonators().subscribe(
      donatorStreamData => {
        this.donatorCount = donatorStreamData.length;
        this.listGroup = this.countGroups(donatorStreamData);
        this.chartDatasets = [
          { data: this.listGroup, label: 'Grupy krwi' }
        ];
      }
    );
  }  


  countGroups(donatorList: DonatorStructure[]){
    let oGroupMinus: number = 0;
    let oGroupPlus: number = 0;
    let aGroupMinus: number = 0;
    let aGroupPlus: number = 0;
    let bGroupMinus: number = 0;
    let bGroupPlus: number = 0;
    let abGroupMinus: number = 0;
    let abGroupPlus: number = 0;

    for (let donator of donatorList){
      if (donator.blood === "0" && donator.antigen === "-"){
        oGroupMinus++
      }
      else if (donator.blood === "0" && donator.antigen === "+"){
        oGroupPlus++
      }
      else if (donator.blood === "A" && donator.antigen === "-"){
        aGroupMinus++
      }
      else if (donator.blood === "A" && donator.antigen === "+"){
        aGroupPlus++
      }
      else if (donator.blood === "B" && donator.antigen === "-"){
        bGroupMinus++
      }
      else if (donator.blood === "B" && donator.antigen === "+"){
        bGroupPlus++
      }
      else if (donator.blood === "AB" && donator.antigen === "-"){
        abGroupMinus++
      }
      else if (donator.blood === "AB" && donator.antigen === "+"){
        abGroupPlus++
      }
    }
    let listGroup: (number|undefined)[] = [oGroupMinus,oGroupPlus,aGroupMinus,aGroupPlus,bGroupMinus,bGroupPlus,abGroupMinus, abGroupPlus];
    return listGroup;
  }


  public chartLabels: Array<any> = ['0-', '0+', 'A-', 'A+', 'B-', 'B+', 'AB-', 'AB+'];

  public chartColors: Array<any> = [
    {
      backgroundColor: [    
      'rgba(217, 171, 171,0.9)',
      'rgba(247, 70, 74, 0.9)',
      'rgba(75, 192, 192, 0.9)',
      'rgba(35, 131, 166, 0.9)',
      'rgba(16, 23, 66, 0.9)',
      'rgba(153, 98, 156, 0.9)',
      'rgba(52, 0, 54,0.9)',
      'rgba(77, 83, 96, 0.9)',],
      hoverBackgroundColor: [
        'rgba(217, 171, 171,1)',
        'rgba(247, 70, 74, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(35, 131, 166, 1)',
        'rgba(16, 23, 66, 1)',
        'rgba(153, 98, 156, 1)',
        'rgba(52, 0, 54,1)',
        'rgba(77, 83, 96, 1)',
      ],
      borderWidth: 2,
    }
  ];

  public chartOptions: any = {
    responsive: true
  };
  public chartClicked(e: any): void { }
  public chartHovered(e: any): void { }

}


