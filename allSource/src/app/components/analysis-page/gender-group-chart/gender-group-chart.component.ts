import { Component, OnInit } from '@angular/core';
import { DonatorStructure } from 'src/app/models/donators-structure';
import { DonatorsDataService } from 'src/app/services/donators-data.service';

@Component({
  selector: 'app-gender-group-chart',
  templateUrl: './gender-group-chart.component.html',
  styleUrls: ['./gender-group-chart.component.css']
})
export class GenderGroupChartComponent{

  donatorCount:number = 0;
  full: number = 0;
  listGroupOne: (number|undefined)[] = [];
  listGroupTwo: (number|undefined)[] = [];
  listGroupThree: (number|undefined)[] = [];
  chartDatasets: Array<any> = [
    { data: [], label: '2 meisiące temu' },
    { data: [], label: '3 miesiące temu' },
    { data: [], label: '1 miesiąc temu' }
  ];

  constructor(private donatorsDataService: DonatorsDataService) {
    donatorsDataService.getDonators().subscribe(
      donatorStreamData => {
        this.listGroupOne = this.groupListOne(donatorStreamData);
        this.listGroupTwo = this.groupListTwo(donatorStreamData);
        this.listGroupThree = this.groupListThree(donatorStreamData);
        this.chartDatasets= [
          { data: this.listGroupOne, label: '1 miesiąc temu' },
          { data: this.listGroupTwo, label: '2 miesiące temu' },
          { data: this.listGroupThree, label: '3 miesiące temu' }
        ];
      }
    );
  }  


  checkDateThree(){
    let todayMonth: number = new Date().getUTCMonth() + 1;
    let todayMonthCheck: string ="";
    let todayYear: number = new Date().getUTCFullYear();
    if(todayMonth <= 3){
      todayMonth = 9 + todayMonth;
      todayYear -= 1;
    } else {
      todayMonth -= 3
    }

    if (todayMonth < 10){
      todayMonthCheck = "0" + todayMonth;
      return todayYear + '-' + todayMonthCheck;
    }
    return todayYear + '-' + todayMonth;
  }



  groupListThree(donatorList: DonatorStructure[]){
    let oGroupMinus: number = 0;
    let oGroupPlus: number = 0;
    let aGroupMinus: number = 0;
    let aGroupPlus: number = 0;
    let bGroupMinus: number = 0;
    let bGroupPlus: number = 0;
    let abGroupMinus: number = 0;
    let abGroupPlus: number = 0;
    for(let donator of donatorList){
      if (donator.last_donate.includes(this.checkDateThree())){
        if (donator.blood === "0" && donator.antigen === "-"){
          oGroupMinus++
        }
        else if (donator.blood === "0" && donator.antigen === "+"){
          oGroupPlus++
        }
        else if (donator.blood === "A" && donator.antigen === "-"){
          aGroupMinus++
        }
        else if (donator.blood === "A" && donator.antigen === "+"){
          aGroupPlus++
        }
        else if (donator.blood === "B" && donator.antigen === "-"){
          bGroupMinus++
        }
        else if (donator.blood === "B" && donator.antigen === "+"){
          bGroupPlus++
        }
        else if (donator.blood === "AB" && donator.antigen === "-"){
          abGroupMinus++
        }
        else if (donator.blood === "AB" && donator.antigen === "+"){
          abGroupPlus++
        }

      }
    }

    let listGroupThree: (number|undefined)[] = [oGroupMinus,oGroupPlus,aGroupMinus,aGroupPlus,bGroupMinus,bGroupPlus,abGroupMinus, abGroupPlus];
    console.log(listGroupThree)
    return listGroupThree;
  }



  checkDateTwo(){
    let todayMonthCheck:string = "";
    let todayMonth: number = new Date().getUTCMonth() + 1;
    let todayYear: number = new Date().getUTCFullYear();
    if(todayMonth <= 2){
      todayMonth = 10 + todayMonth;
      todayYear -= 1;
    } else {
      todayMonth -= 2
    }
    if (todayMonth < 10){
      todayMonthCheck = "0" + todayMonth;
      return todayYear + '-' + todayMonthCheck;
    }
    return todayYear + '-' + todayMonth;
  }

  groupListTwo(donatorList: DonatorStructure[]){
    let oGroupMinus: number = 0;
    let oGroupPlus: number = 0;
    let aGroupMinus: number = 0;
    let aGroupPlus: number = 0;
    let bGroupMinus: number = 0;
    let bGroupPlus: number = 0;
    let abGroupMinus: number = 0;
    let abGroupPlus: number = 0;
    for(let donator of donatorList){
      if (donator.last_donate.includes(this.checkDateTwo())){
        if (donator.blood === "0" && donator.antigen === "-"){
          oGroupMinus++
        }
        else if (donator.blood === "0" && donator.antigen === "+"){
          oGroupPlus++
        }
        else if (donator.blood === "A" && donator.antigen === "-"){
          aGroupMinus++
        }
        else if (donator.blood === "A" && donator.antigen === "+"){
          aGroupPlus++
        }
        else if (donator.blood === "B" && donator.antigen === "-"){
          bGroupMinus++
        }
        else if (donator.blood === "B" && donator.antigen === "+"){
          bGroupPlus++
        }
        else if (donator.blood === "AB" && donator.antigen === "-"){
          abGroupMinus++
        }
        else if (donator.blood === "AB" && donator.antigen === "+"){
          abGroupPlus++
        }

      }
    }
    let listGroupTwo: (number|undefined)[] = [oGroupMinus,oGroupPlus,aGroupMinus,aGroupPlus,bGroupMinus,bGroupPlus,abGroupMinus, abGroupPlus];
    console.log(listGroupTwo);
    return listGroupTwo;
  }



  checkDateOne(){
    let todayMonth: number = new Date().getUTCMonth() + 1;
    let todayMonthCheck:string = "";
    let todayYear: number = new Date().getUTCFullYear();
    if(todayMonth === 1){
      todayMonth += 11;
      todayYear -= 1;
    } else {
      todayMonth -= 1
    }
    if (todayMonth < 10){
      todayMonthCheck = "0" + todayMonth;
      return todayYear + '-' + todayMonthCheck;
    }
    return todayYear + '-' + todayMonth;
  }

  groupListOne(donatorList: DonatorStructure[]){
    let oGroupMinus: number = 0;
    let oGroupPlus: number = 0;
    let aGroupMinus: number = 0;
    let aGroupPlus: number = 0;
    let bGroupMinus: number = 0;
    let bGroupPlus: number = 0;
    let abGroupMinus: number = 0;
    let abGroupPlus: number = 0;
    for(let donator of donatorList){
      if (donator.last_donate.includes(this.checkDateOne())){
        if (donator.blood === "0" && donator.antigen === "-"){
          oGroupMinus++
        }
        else if (donator.blood === "0" && donator.antigen === "+"){
          oGroupPlus++
        }
        else if (donator.blood === "A" && donator.antigen === "-"){
          aGroupMinus++
        }
        else if (donator.blood === "A" && donator.antigen === "+"){
          aGroupPlus++
        }
        else if (donator.blood === "B" && donator.antigen === "-"){
          bGroupMinus++
        }
        else if (donator.blood === "B" && donator.antigen === "+"){
          bGroupPlus++
        }
        else if (donator.blood === "AB" && donator.antigen === "-"){
          abGroupMinus++
        }
        else if (donator.blood === "AB" && donator.antigen === "+"){
          abGroupPlus++
        }

      }
    }
    let listGroupOne: (number|undefined)[] = [oGroupMinus,oGroupPlus,aGroupMinus,aGroupPlus,bGroupMinus,bGroupPlus,abGroupMinus, abGroupPlus];
    return listGroupOne;
  }



  listMonth: string [] = ["0-", "0+", "A-", "A-", "B-", "B+", "AB-", "AB+"];

  public chartType: string = 'line';
  
  checkMonthOne(){
    let todayMonth: number = new Date().getUTCMonth()+1;
    if ((todayMonth - 1)<1){
     return todayMonth += 11;
    }
    return todayMonth-1;
  }

  checkMonthTwo(){
    let todayMonth: number = new Date().getUTCMonth()+1;
    if ((todayMonth - 2)<1){
     return todayMonth += 10;
    }
    return todayMonth-2;
  }

  checkMonthThree(){
    let todayMonth: number = new Date().getUTCMonth()+1;
    if ((todayMonth - 3)<1){
     return todayMonth += 9;
    }
    return todayMonth-3;
  }

  public chartLabels: Array<any> = ["0-", "0+", "A-", "A-", "B-", "B+", "AB-", "AB+"];


  public chartColors: Array<any> = [
    {
      backgroundColor: 'rgba(255, 249, 61, .2)',
      borderColor: '#d6d00f',
      borderWidth: 2,
    },
    {
      backgroundColor: 'rgba(70, 191, 189, .2)',
      borderColor: '#46BFBD',
      borderWidth: 2,
    },
    {
      backgroundColor: 'rgba(74, 10, 82, .2)',
      borderColor: '#4a0a52',
      borderWidth: 2,
    }
  ];

  public chartOptions: any = {
    responsive: true,
    scales: {
      yAxes: [{
          ticks: {
            stepSize: 1,
            beginAtZero: true,
          }
      }]
    }
  };
  public chartClicked(e: any): void { }
  public chartHovered(e: any): void { }

}