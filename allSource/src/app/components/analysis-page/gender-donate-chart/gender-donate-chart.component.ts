import { ThisReceiver } from '@angular/compiler';
import { Component, Input, OnInit } from '@angular/core';
import { values } from 'lodash';
import { DonatorStructure } from 'src/app/models/donators-structure';
import { DonatorsDataService } from 'src/app/services/donators-data.service';


@Component({
  selector: 'app-gender-donate-chart',
  templateUrl: './gender-donate-chart.component.html',
  styleUrls: ['./gender-donate-chart.component.css']
})
export class GenderDonateChartComponent implements OnInit {

  donatorCount: number = 0;
  listFemale: number[] = [];
  listMale:Array<number> = [];
  chartDatasets: Array<any> = [
    { data: [], label: 'Kobiety' },
    { data: [], label: 'Mężczyźni' }
  ];

    constructor(private donatorsDataService: DonatorsDataService) {

      donatorsDataService.getDonators().subscribe(
        donatorStreamData => {
          this.donatorCount = donatorStreamData.length;
          this.listFemale = this.countFemale(donatorStreamData);
          this.listMale = this.countMale(donatorStreamData);
          this.chartDatasets = [
            { data: this.listFemale, label: 'Kobiety' },
            { data: this.listMale, label: 'Mężczyźni' }
          ];
        }
      );
    }  
  ngOnInit(): void {
  }
  

  checkDateOne(){
    let todayMonth: number = new Date().getUTCMonth() + 1;
    let todayYear: number = new Date().getUTCFullYear();
    if(todayMonth === 1){
      todayMonth = 11 + todayMonth;
      todayYear -= 1;
    } else {
      todayMonth -= 1
    }
    return todayYear + '-' + todayMonth;
  }

  checkDateTwo(){
    let todayMonth: number = new Date().getUTCMonth() + 1;
    let todayYear: number = new Date().getUTCFullYear();
    if(todayMonth <= 2){
      todayMonth = 10 + todayMonth;
      todayYear -= 1;
    } else {
      todayMonth -= 2
    }
    return todayYear + '-' + todayMonth;
  }

  checkDateThree(){
    let todayMonthCheck: string = "";
    let todayMonth: number = new Date().getUTCMonth() + 1;
    let todayYear: number = new Date().getUTCFullYear();
    if(todayMonth <= 3){
      todayMonth = 9 + todayMonth;
      todayYear -= 1;
    } else {
      todayMonth -= 3
    }
    if (todayMonth < 10){
      todayMonthCheck = "0" + todayMonth;
      return todayYear + '-' + todayMonthCheck;
    }
    return todayYear + '-' + todayMonth;
  }

  countFemale (donatorList: DonatorStructure[]){
    let femaleCountOne: number = 0;
    let femaleCountTwo: number = 0;
    let femaleCountThree: number = 0;
    for(let donator of donatorList){
      if (donator.gender === "kobieta"){
        if (donator.last_donate.includes(this.checkDateOne())){
          femaleCountOne++
        }
        else if  (donator.last_donate.includes(this.checkDateTwo())){
          femaleCountTwo++
        }
        else if  (donator.last_donate.includes(this.checkDateThree())){
          femaleCountThree++
        }
      }
    }
    let listFemale:Array<number> = [femaleCountThree , femaleCountTwo, femaleCountOne];
    console.log("female3: ", femaleCountThree);
    return listFemale;
  }
  

  countMale (donatorList: DonatorStructure[]){
    let maleCountOne: number = 0;
    let maleCountTwo: number = 0;
    let maleCountThree: number = 0;
    for(let donator of donatorList){
      if (donator.gender === "mężczyzna"){
        if (donator.last_donate.includes(this.checkDateOne())){
          maleCountOne++
        }
        else if  (donator.last_donate.includes(this.checkDateTwo())){
          maleCountTwo++
        }
        else if  (donator.last_donate.includes(this.checkDateThree())){
          maleCountThree++
        }
      }
    }
    let listMale:Array<number> = [maleCountThree , maleCountTwo, maleCountOne];
    console.log("male3: ", maleCountThree);
    return listMale;
  }

  listMonth: string [] = ["styczeń", "luty", "marzec", "kwiecień", "maj", "czerwiec", "lipiec", "sierpień", "wrzesień", "październik", "listopad", "grudzień"];

  public chartType: string = 'line';
  
  checkMonthOne(){
    let todayMonth: number = new Date().getUTCMonth()+1;
    if ((todayMonth - 1)<1){
     return todayMonth += 11;
    }
    return todayMonth-1;
  }

  checkMonthTwo(){
    let todayMonth: number = new Date().getUTCMonth()+1;
    if ((todayMonth - 2)<1){
     return todayMonth += 10;
    }
    return todayMonth-2;
  }

  checkMonthThree(){
    let todayMonth: number = new Date().getUTCMonth()+1;
    if ((todayMonth - 3)<1){
     return todayMonth += 9;
    }
    return todayMonth-3;
  }


  public chartLabels: Array<any> = [this.listMonth[this.checkMonthThree()-1], this.listMonth[this.checkMonthTwo()-1], this.listMonth[this.checkMonthOne()-1]];

  public chartColors: Array<any> = [
    {
      backgroundColor: 'rgba(74, 10, 82, .2)',
      borderColor: 'rgba(74, 10, 82, .7)',
      borderWidth: 2,
    },
    {
      backgroundColor: 'rgba(0, 137, 132, .2)',
      borderColor: 'rgba(0, 10, 130, .7)',
      borderWidth: 2,
    }
  ];

  public chartOptions: any = {
    responsive: true,
    scales: {
      yAxes: [{
          ticks: {
            stepSize: 1,
            beginAtZero: true,
          }
      }]
    }
  };
  public chartClicked(e: any): void { }
  public chartHovered(e: any): void { }

}