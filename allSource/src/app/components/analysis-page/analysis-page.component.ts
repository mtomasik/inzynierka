import { Component } from "@angular/core";

@Component({
  selector: 'app-analysis-page',
  templateUrl: './analysis-page.component.html',
  styleUrls: ['./analysis-page.component.css']
})
export class AnalysisPageComponent{

  checkMonthOne(){
    let todayMonth: number = new Date().getUTCMonth()+1;
    if ((todayMonth - 2)<0){
     return todayMonth += 10;
    }
    return todayMonth-2;
  }

  checkMonthTwo(){
    let todayMonth: number = new Date().getUTCMonth()+1;
    if ((todayMonth - 3)<0){
     return todayMonth += 9;
    }
    return todayMonth-3;
  }

  checkMonthThree(){
    let todayMonth: number = new Date().getUTCMonth()+1;
    if ((todayMonth - 4)<0){
     return todayMonth += 8;
    }
    return todayMonth-4;
  }

  listMonth: string [] = ["styczeń", "luty", "marzec", "kwiecień", "maj", "czerwiec", "lipiec", "sierpień", "wrzesień", "październik", "listopad", "grudzień"];



}

