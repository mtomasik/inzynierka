import { Component, OnInit } from '@angular/core';
import { DonatorStructure } from 'src/app/models/donators-structure';
import { DonatorsDataService } from 'src/app/services/donators-data.service';

@Component({
  selector: 'app-month-ago-chart',
  templateUrl: './month-ago-chart.component.html',
  styleUrls: ['./month-ago-chart.component.css']
})
export class MonthAgoChartComponent {
  public chartType: string = 'bar';
  listGroup: (number|undefined)[] = [];
  chartDatasets: Array<any> = [
    { data: [], label: 'Grupy krwi' }
  ];

  constructor(private donatorsDataService: DonatorsDataService) {

    donatorsDataService.getDonators().subscribe(
      donatorStreamData => {
        this.listGroup = this.groupList(donatorStreamData);
        this.chartDatasets= [
          { data: this.listGroup, label: 'My First dataset' }
        ];
      }
    );
  }  


  checkDate(){
    let todayMonth: number = new Date().getUTCMonth() + 1;
    let todayMonthCheck:string = "";
    let todayYear: number = new Date().getUTCFullYear();
    if(todayMonth === 1){
      todayMonth += 11;
      todayYear -= 1;
    } else {
      todayMonth -= 1
    }
    if (todayMonth < 10){
      todayMonthCheck = "0" + todayMonth;
      return todayYear + '-' + todayMonthCheck;
    }
    return todayYear + '-' + todayMonth;
  }

  groupList(donatorList: DonatorStructure[]){
    let oGroupMinus: number = 0;
    let oGroupPlus: number = 0;
    let aGroupMinus: number = 0;
    let aGroupPlus: number = 0;
    let bGroupMinus: number = 0;
    let bGroupPlus: number = 0;
    let abGroupMinus: number = 0;
    let abGroupPlus: number = 0;
    for(let donator of donatorList){
      if (donator.last_donate.includes(this.checkDate())){
        if (donator.blood === "0" && donator.antigen === "-"){
          oGroupMinus++
        }
        else if (donator.blood === "0" && donator.antigen === "+"){
          oGroupPlus++
        }
        else if (donator.blood === "A" && donator.antigen === "-"){
          aGroupMinus++
        }
        else if (donator.blood === "A" && donator.antigen === "+"){
          aGroupPlus++
        }
        else if (donator.blood === "B" && donator.antigen === "-"){
          bGroupMinus++
        }
        else if (donator.blood === "B" && donator.antigen === "+"){
          bGroupPlus++
        }
        else if (donator.blood === "AB" && donator.antigen === "-"){
          abGroupMinus++
        }
        else if (donator.blood === "AB" && donator.antigen === "+"){
          abGroupPlus++
        }

      }
    }
    let listGroup: (number|undefined)[] = [oGroupMinus,oGroupPlus,aGroupMinus,aGroupPlus,bGroupMinus,bGroupPlus,abGroupMinus, abGroupPlus];
    return listGroup;
  }

  public chartLabels: Array<any> = ['0-', '0+', 'A-', 'A+', 'B-', 'B+', 'AB-', 'AB+'];

  public chartColors: Array<any> = [
    {
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(255, 159, 64, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(181, 34, 186, 0.2)',
        'rgba(77, 83, 96, 0.2)'
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(255, 159, 64, 1)',
        'rgba(153, 102, 255, 1)',
        'rgba(181, 34, 186,1)',
        'rgba(77, 83, 96, 1)',
      ],
      borderWidth: 2,
    }
  ];

  public chartOptions: any = {
    responsive: true,
    scales: {
      yAxes: [{
          ticks: {
            stepSize: 1,
            beginAtZero: true,
          }
      }]
  }
  };
  public chartClicked(e: any): void { }
  public chartHovered(e: any): void { }
}
