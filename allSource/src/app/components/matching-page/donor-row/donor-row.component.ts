import { Component, Input} from '@angular/core';
import { DonatorStructure } from 'src/app/models/donators-structure';

@Component({
  selector: 'app-donor-row',
  templateUrl: './donor-row.component.html',
  styleUrls: ['./donor-row.component.css']
})
export class DonorRowComponent {
  @Input() donator: DonatorStructure = {
    id: "",
    name: "",
    lastname: "",
    gender: "",
    id_person: "",
    email: "",
    phone: "",
    blood: "",
    antigen: "",
    last_donate: "",
    code: "",
    city: "",
    address: "",
    about: ""}


  constructor() { }


}
