import { Component, Input, OnInit, Output } from '@angular/core';
import { DonatorStructure } from 'src/app/models/donators-structure';
import { DonatorsDataService } from 'src/app/services/donators-data.service';
import cloneDeep from 'lodash-es/cloneDeep'
import { ActivatedRoute, ParamMap, Router} from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-donor-details',
  templateUrl: './donor-details.component.html',
  styleUrls: ['./donor-details.component.css']
})
export class DonorDetailsComponent implements OnInit {

  public cssClass: string = 'e-custom-style';
  donator: DonatorStructure = {
    id: "",
    name: "",
    lastname: "",
    gender: "",
    id_person: "",
    email: "",
    phone: "",
    blood: "",
    antigen: "",
    last_donate: "",
    code: "",
    city: "",
    address: "",
    about: ""}

  inBox = false;
  editMode = false;
  public popoverTitle:string = "Usuń dawcę krwi";
  public popoverMessage:string = "Czy jesteś pewny, że chcesz usunąć użytkownika";
  public confirmClicked:boolean = false;
  public cancelClicked:boolean = false;
  model: Partial<DonatorStructure>={}; //inicjalizacja jako obiekt pusty, ponieważ będziemy się odwoływać przez bindowanie. Inaczej wyskoczy błąd

  donatorTemp: DonatorStructure = {
    id: "",
    name: "",
    lastname: "",
    gender: "",
    id_person: "",
    email: "",
    phone: "",
    blood: "",
    antigen: "",
    last_donate: "",
    code: "",
    city: "",
    address: "",
    about: ""}

    duplicate: DonatorStructure = {
      id: "",
      name: "",
      lastname: "",
      gender: "",
      id_person: "",
      email: "",
      phone: "",
      blood: "",
      antigen: "",
      last_donate: "",
      code: "",
      city: "",
      address: "",
      about: ""}

  datePickerValue = {
    year: "",
    month: "",
    day: ""
  }


  donatorDetails?: Observable<DonatorStructure>;

  ngOnInit() {
    

  }

  constructor(private donatorsDataService: DonatorsDataService, private route:ActivatedRoute,
    private router: Router){
      this.donatorDetails = this.route.paramMap.pipe(
        switchMap((params: ParamMap) => this.donatorsDataService.getDonator(params.get('id')|| '{}'))
      );
      this.donatorDetails.subscribe(
        donatorData => this.donator = donatorData
      )
    }

  getGender(gender: string){
    if (gender==="kobieta") return true;
    return false;
  }

  calculateDiff(last_donate: string){
    let currentDate = new Date();

    let days = Math.floor((currentDate.getTime() - new Date(last_donate).getTime()) / 1000 / 60 / 60 / 24);
    if (days>56) return true;
    return false;
  }

  over(){
    this.inBox = true;
  }

  out(){
    this.inBox = false;
  }
  
  delete() 
      {
        this.donatorsDataService.deleteDonator(this.donator.id).subscribe();
        this.router.navigate(['/matching-page']);
      }; 

  patch() {
    const donator: Partial<DonatorStructure> = {};
    this.donatorsDataService.patchDonator(this.donator).subscribe();
    }

    switchEditMode(){
      this.editMode = !this.editMode; 
      this.donatorTemp = cloneDeep(this.donator); 
      this.duplicate = cloneDeep(this.donator);

    }

    addZero(x:string) {
      if(x.toString().length < 2) return "0"+x;
      return x;
    }

    save(){
      this.editMode = false;
      this.donator = cloneDeep(this.donatorTemp);
      this.donator.last_donate = this.datePickerValue.year + "-" + this.addZero(this.datePickerValue.month) +"-"+ this.addZero(this.datePickerValue.day);
      this.patch();
    }

    cancel(){
      this.donator = cloneDeep(this.duplicate);
      this.editMode = false;
    }
}