import { Component, Input, OnInit } from '@angular/core';
import { DonatorStructure } from 'src/app/models/donators-structure';
import { DonatorsDataService } from 'src/app/services/donators-data.service';
import cloneDeep from 'lodash-es/cloneDeep';

@Component({
  selector: 'app-matching-page',
  templateUrl: './matching-page.component.html',
  styleUrls: ['./matching-page.component.css']
})
export class MatchingPageComponent implements OnInit {
  donatorStream$: DonatorStructure[] = [];
  @Input() donator: DonatorStructure = {
    id: "",
    name: "",
    lastname: "",
    gender: "",
    id_person: "",
    email: "",
    phone: "",
    blood: "",
    antigen: "",
    last_donate: "",
    code: "",
    city: "",
    address: "",
    about: ""}

  dictionary_blood: {[key: string]: string}= {
    "0": '[0]',
    "A": '[0,A]',
    "B" : '[0,B]',
    "AB" : '[0, A, B, AB]'
  };

  dictionary_antigen: {[key: string]: string}= {
    "-": '[-]',
    "+": '[-,+]',
  };

  filteredDonors: DonatorStructure[] = [];
  
  matchingMode=false;
  mobile=false;
  antigenCategories: string[] = [];
  bloodCategories: string[] = [];
  donatorStructure: Partial<DonatorStructure> = {};
  matchingValues = {
    blood: "",
    antigen: ""};

    constructor(private donatorsDataService: DonatorsDataService) {
      donatorsDataService.getDonators().subscribe(
      donatorStreamData => (this.donatorStream$ = donatorStreamData,
        this.filteredDonors = cloneDeep(this.donatorStream$))
      );
      window.onresize = (event) =>
      {
        if (window.innerWidth <= 575) {
          this.mobile = true;
        }
        else this.mobile = false;
      }
    }

    ngOnInit(): void {
      this.donatorsDataService.getAntigen().subscribe(antigenCategories => this.antigenCategories = antigenCategories);
      this.donatorsDataService.getBlood().subscribe(bloodCategories => this.bloodCategories = bloodCategories);
    }

    matchingFunction(){
      this.filteredDonors = cloneDeep(this.donatorStream$);
      this.matchingMode=false;
      if (this.matchingValues.blood != ""){
        this.filteredDonors = this.filteredDonors.filter(donator => this.dictionary_blood[this.matchingValues.blood].includes(donator.blood));
        this.matchingMode = true
      }
      if (this.matchingValues.antigen != ""){
        this.filteredDonors = this.filteredDonors.filter(donator => this.dictionary_antigen[this.matchingValues.antigen].includes(donator.antigen));
        this.matchingMode = true
      }
    }

    clear_blood(){
      this.matchingValues.blood = "";
      this.matchingFunction();
    }

    clear_antigen(){
      this.matchingValues.antigen = "";
      this.matchingFunction();
    }
}
