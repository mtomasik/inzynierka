import { Component } from '@angular/core';
import { DonatorStructure } from 'src/app/models/donators-structure';
import { DonatorsDataService } from 'src/app/services/donators-data.service';

@Component({
  selector: 'app-donators-page',
  templateUrl: './donators-page.component.html',
  styleUrls: ['./donators-page.component.css']
})
export class DonatorsPageComponent {

  donatorStream$: DonatorStructure[] = [];

  constructor(private donatorsDataService: DonatorsDataService) {

    donatorsDataService.getDonators().subscribe(
      donatorStreamData => this.donatorStream$ = donatorStreamData
    );

  }
  refresh(status: boolean) {
    this.donatorsDataService.getDonators().subscribe(
      donatorStreamData => {
        console.log(donatorStreamData)
        this.donatorStream$ = donatorStreamData
      }
    );
  }

}


