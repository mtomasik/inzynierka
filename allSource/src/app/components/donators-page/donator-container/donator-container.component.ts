import { Component, EventEmitter, Input, Output } from '@angular/core';
import { DonatorStructure } from 'src/app/models/donators-structure';
import { DonatorsDataService } from 'src/app/services/donators-data.service';
import cloneDeep from 'lodash-es/cloneDeep'

@Component({
  selector: 'app-donator-container',
  templateUrl: './donator-container.component.html',
  styleUrls: ['./donator-container.component.css']
})

export class DonatorContainerComponent {
  @Output() refresh = new EventEmitter<boolean>();
  public cssClass: string = 'e-custom-style';
  @Input() donator: DonatorStructure = {
    id: "",
    name: "",
    lastname: "",
    gender: "",
    id_person: "",
    email: "",
    phone: "",
    blood: "",
    antigen: "",
    last_donate: "",
    code: "",
    city: "",
    address: "",
    about: ""
  }

  inBox = false;
  moreMode = false;
  editMode = false;
  errorBox = false;
  public popoverTitle: string = "Usuń dawcę krwi";
  public popoverMessage: string = "Czy jesteś pewny, że chcesz usunąć użytkownika";
  public confirmClicked: boolean = false;
  public cancelClicked: boolean = false;
  model: Partial<DonatorStructure> = {}; //inicjalizacja jako obiekt pusty, ponieważ będziemy się odwoływać przez bindowanie. Inaczej wyskoczy błąd

  donatorTemp: DonatorStructure = {
    id: "",
    name: "",
    lastname: "",
    gender: "",
    id_person: "",
    email: "",
    phone: "",
    blood: "",
    antigen: "",
    last_donate: "",
    code: "",
    city: "",
    address: "",
    about: ""
  }

  duplicate: DonatorStructure = {
    id: "",
    name: "",
    lastname: "",
    gender: "",
    id_person: "",
    email: "",
    phone: "",
    blood: "",
    antigen: "",
    last_donate: "",
    code: "",
    city: "",
    address: "",
    about: ""
  }

  datePickerValue = {
    year: "",
    month: "",
    day: ""
  }

  constructor(private donatorsDataService: DonatorsDataService) { }

  getGender(gender: string) {
    if (gender === "kobieta") return true;
    return false;
  }

  calculateDiff(last_donate: string) {
    let currentDate = new Date();

    let days = Math.floor((currentDate.getTime() - new Date(last_donate).getTime()) / 1000 / 60 / 60 / 24);
    if (days > 56) return true;
    return false;
  }

  over() {
    this.inBox = true;
  }

  out() {
    this.inBox = false;
  }

  switchMoreMode() {
    this.moreMode = !this.moreMode;
  }

  delete() {
    this.donatorsDataService.deleteDonator(this.donator.id).subscribe(
      resp => { if (resp != "") this.refresh.emit(true) }
    );
  };

  patch() {
    const donator: Partial<DonatorStructure> = {};
    this.donatorsDataService.patchDonator(this.donator).subscribe();
  }

  switchEditMode() {
    this.editMode = !this.editMode;
    this.duplicate = cloneDeep(this.donator);
    this.donatorTemp = this.donator;
  }

  addZero(x: string) {
    if (x.toString().length < 2) return "0" + x;
    return x;
  }

  errorGap: string = "";
  alert: string = "";

  save() {
    if (this.datePickerValue.month === ("")) {
      this.errorGap = "'Ostatnie pobranie'";
      this.errorBox = true;
    }
    else if (this.donatorTemp.phone === "") {
      this.errorGap = "'Numer telefonu'";
      this.errorBox = true;
    }
    else if (this.donatorTemp.email === "") {
      this.errorGap = "'Email'";
      this.errorBox = true;
    }
    else if (!this.donatorTemp.email.includes("@")) {
      this.errorGap = "'Email' w prawidłowym formacie";
      this.errorBox = true;
    }
    else if (this.donatorTemp.id_person === "") {
      this.errorGap = "'PESEL'";
      this.errorBox = true;
    }
    else {
      this.errorBox = false;;
      this.editMode = false;
      this.donator.last_donate = this.datePickerValue.year + "-" + this.addZero(this.datePickerValue.month) + "-" + this.addZero(this.datePickerValue.day);
      this.patch();
    }
    this.alert = `Pole ${this.errorGap} jest wymagane`;
  }

  cancel() {
    this.errorBox = false;
    this.donator = cloneDeep(this.duplicate);
    this.editMode = false;
  }
}

