import { Component, EventEmitter, OnInit, Output} from '@angular/core';
import { DonatorStructure } from 'src/app/models/donators-structure';
import { DonatorsDataService } from 'src/app/services/donators-data.service';

@Component({
  selector: 'app-new-donator',
  templateUrl: './new-donator.component.html',
  styleUrls: ['./new-donator.component.css']
})
export class NewDonatorComponent implements OnInit {
  @Output() refresh = new EventEmitter<boolean>();
  addMode = false;
  donatorStructure: Partial<DonatorStructure> = {};
  donatorTemp: DonatorStructure = {
    id: "",
    name: "",
    lastname: "",
    gender: "",
    id_person: "",
    email: "",
    phone: "",
    blood: "",
    antigen: "",
    last_donate: "",
    code: "",
    city: "",
    address: "",
    about: ""}
  donatorsList: any;

  addFunction(){
    this.addMode=!this.addMode;
    this.errorBox=false;
  }

  datePickerValue = {
    year: "",
    month: "",
    day: ""
  }

  antigenCategories: string[] = [];
  bloodCategories: string[] = [];
  genderCategories: string[] = [];

  constructor(private donatorsDataService: DonatorsDataService) {}

  ngOnInit(): void {
    this.donatorsDataService.getGender().subscribe(genderCategories => this.genderCategories = genderCategories);
    this.donatorsDataService.getAntigen().subscribe(antigenCategories => this.antigenCategories = antigenCategories);
    this.donatorsDataService.getBlood().subscribe(bloodCategories => this.bloodCategories = bloodCategories);
  }

  send() {
    this.donatorsDataService.postDonator(this.donatorTemp).subscribe(
      () =>
        this.refresh.emit(true)
    );
  }

  addZero(x:string) {
    if(x.toString().length < 2) return "0"+x;
    return x;
  }

  errorGap: string = "";
  alert:string = "";
  errorBox:boolean = false;

  save(){
    if (this.donatorTemp.name === ""){
      this.errorGap = "'Imię'";
      this.errorBox = true;
    }
    else if (this.donatorTemp.lastname === ""){
      this.errorGap = "'Nazwisko'";
      this.errorBox = true;
    }
    else if (this.donatorTemp.id_person === ""){
      this.errorGap = "'PESEL'";
      this.errorBox = true;
    }
    else if (this.donatorTemp.gender === ""){
      this.errorGap = "'Płeć'";
      this.errorBox = true;
    }
    else if (this.donatorTemp.blood === ""){
      this.errorGap = "'Grupa krwi'";
      this.errorBox = true;
    }
    else if (this.donatorTemp.antigen === ""){
      this.errorGap = "'Antygen'";
      this.errorBox = true;
    }
    else if (this.datePickerValue.month === ("")){
      this.errorGap = "'Ostatnie pobranie'";
      this.errorBox = true;
    }
    else if (this.donatorTemp.phone === ""){
      this.errorGap = "'Numer telefonu'";
      this.errorBox = true;
    }
    else if (this.donatorTemp.email === ""){
      this.errorGap = "'Email'";
      this.errorBox = true;
    }
    else if (!this.donatorTemp.email.includes("@")){
      this.errorGap = "'Email' w prawidłowym formacie";
      this.errorBox = true;
    }
    else{
      this.donatorTemp.last_donate = this.datePickerValue.year + "-" + this.addZero(this.datePickerValue.month) +"-"+ this.addZero(this.datePickerValue.day);
      this.addFunction();
      this.send();
      this.donatorTemp  = {
        id: "",
        name: "",
        lastname: "",
        gender: "",
        id_person: "",
        email: "",
        phone: "",
        blood: "",
        antigen: "",
        last_donate: "",
        code: "",
        city: "",
        address: "",
        about: ""}
    }
    this.alert = `Pole ${this.errorGap} jest wymagane`;
  }

  cancel(){
    this.addFunction();
    this.donatorTemp  = {
      id: "",
      name: "",
      lastname: "",
      gender: "",
      id_person: "",
      email: "",
      phone: "",
      blood: "",
      antigen: "",
      last_donate: "",
      code: "",
      city: "",
      address: "",
      about: ""}
  }
}
