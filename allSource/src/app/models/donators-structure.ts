export interface DonatorStructure {
    id: string;
    name: string;
    lastname: string;
    gender: string;
    id_person: string;
    email: string;
    phone: string;
    blood: string;
    antigen: string;
    last_donate: string;
    code: string;
    city: string;
    address: string;
    about: string;
  }