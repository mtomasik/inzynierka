import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DonatorsPageComponent } from './components/donators-page/donators-page.component';
import { AppNavbarComponent } from './components/app-navbar/app-navbar.component';
import { AppRoutingModule } from './app-routing.module';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { StartPageComponent } from './components/start-page/start-page.component';
import { HttpClientModule } from '@angular/common/http';
import { DonatorContainerComponent } from './components/donators-page/donator-container/donator-container.component';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { FormsModule } from '@angular/forms';
import { NewDonatorComponent } from './components/donators-page/new-donator/new-donator.component';
import { MatchingPageComponent } from './components/matching-page/matching-page.component';
import { DonorRowComponent } from './components/matching-page/donor-row/donor-row.component';
import { DonorDetailsComponent } from './components/matching-page/donor-row/donor-details/donor-details.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { AnalysisPageComponent } from './components/analysis-page/analysis-page.component';
import { GenderDonateChartComponent } from './components/analysis-page/gender-donate-chart/gender-donate-chart.component';
import { DonorsBloodChartComponent } from './components/analysis-page/donors-blood-chart/donors-blood-chart.component';
import { DonorsBloodPieComponent } from './components/analysis-page/donors-blood-pie/donors-blood-pie.component';
import { MonthAgoChartComponent } from './components/analysis-page/month-ago-chart/month-ago-chart.component';
import { TwoMonthAgoChartComponent } from './components/analysis-page/two-month-ago-chart/two-month-ago-chart.component';
import { ThreeMonthAgoChartComponent } from './components/analysis-page/three-month-ago-chart/three-month-ago-chart.component';
import { GenderGroupChartComponent } from './components/analysis-page/gender-group-chart/gender-group-chart.component';
import { GenderPieComponent } from './components/analysis-page/gender-pie/gender-pie.component';

@NgModule({
  declarations: [
    AppComponent,
    AppNavbarComponent,
    DonatorsPageComponent,
    PageNotFoundComponent,
    StartPageComponent,
    DonatorContainerComponent,
    NewDonatorComponent,
    MatchingPageComponent,
    DonorRowComponent,
    DonorDetailsComponent,
    AnalysisPageComponent,
    GenderDonateChartComponent,
    DonorsBloodChartComponent,
    DonorsBloodPieComponent,
    MonthAgoChartComponent,
    TwoMonthAgoChartComponent,
    ThreeMonthAgoChartComponent,
    GenderGroupChartComponent,
    GenderPieComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgbModule,
    FormsModule,
    AppRoutingModule,
    ConfirmationPopoverModule.forRoot({
      confirmButtonType: 'danger',
    }),
    MDBBootstrapModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
